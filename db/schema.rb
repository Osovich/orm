module Schema
    def self.create
        ActiveRecord::Schema.define do
            create_table :movies, force: true do |t|
                t.string :name
            end
        end
    end
end